package com.android.vsshr.home.notifications

import com.android.vsshr.network.WebCall
import io.reactivex.Observable

class NotificationsRepository {

    /**The singleton BackEndApi object that is created lazily when the first time it is used
     * After that it will be reused without creation
     */
    private val apiServices by lazy { WebCall.create() }


    companion object {
        fun getInstance(): NotificationsRepository {
            val mInstance: NotificationsRepository by lazy { NotificationsRepository() }
            return mInstance
        }
    }
}