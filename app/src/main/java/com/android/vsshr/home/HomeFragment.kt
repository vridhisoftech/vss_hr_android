package com.android.vsshr.home

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.SystemClock
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.android.vsshr.R
import com.android.vsshr.databinding.FragmentHomeBinding
import com.android.vsshr.factory.ViewModelFactory
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class HomeFragment : Fragment(), HomeCommand {

    private val REQUEST_LOCATION = 1

    private var locationManager: LocationManager? = null
    var lat: String? = null
    var lag: String? = null

    private lateinit var viewModel: HomeViewModel
    private lateinit var binding: FragmentHomeBinding
    private var breakTimeOffset: Long = 0
    private var elapsedTotaltime: Long = 0


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        // With ViewModelFactory
        viewModel = ViewModelProvider(this@HomeFragment, ViewModelFactory()).get(
            HomeViewModel::class.java
        )

        binding.viewModel = viewModel

        binding.inTimer.format = "%s\nMM:SS"
        binding.inTimer.base = SystemClock.elapsedRealtime()

        binding.breakTimer.format = "%s\nMM:SS\nEnd Break"
        binding.breakTimer.base = SystemClock.elapsedRealtime()

        enableDisableClock(false)

        registerObservables()

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        locationManager = (requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager?)
        if (!locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMsgNoGps()
        } else if (locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            viewModel.location.set("Location: "+getLocation())
        }

    }

    private fun getLocation(): String {
        var address: String = ""
        if (ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_LOCATION
            )
        } else {
            val location: Location? =
                locationManager!!.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
            if (location != null) {
                val lati: Double = location.latitude
                val longi: Double = location.longitude
                lat = lati.toString()
                lag = longi.toString()
                address = getAddress(lati, longi)!!
            }
        }

        return address
    }

    private fun getAddress(lat: Double, lag: Double): String? {
        val geocoder = Geocoder(requireContext(), Locale.getDefault())
        var addresses: List<Address>? =
            null // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        try {
            addresses = geocoder.getFromLocation(lat, lag, 1)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        Log.v("FullLocation", addresses!!.toString())
//        address = addresses!![0]
//            .getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        val address = addresses[0].getAddressLine(0)
        Log.v("FullLocation", address)

        return address
    }

    protected fun buildAlertMsgNoGps() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
        builder.setMessage("Please Turn On Your Gps")
            .setCancelable(false)
            .setPositiveButton("Yes",
                DialogInterface.OnClickListener { dialogInterface, i ->
                    startActivity(
                        Intent(
                            Settings.ACTION_LOCATION_SOURCE_SETTINGS
                        )
                    )
                })
            .setNegativeButton("No",
                DialogInterface.OnClickListener { dialogInterface, i -> dialogInterface.cancel() })
        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()
    }

    private fun enableDisableClock(isEnable: Boolean) {
        binding.clockOutParent.alpha = if (isEnable) 1.0f else 0.5f
        binding.clockOutParent.isUserInteractionEnabled(isEnable)

        binding.outBreakParent.alpha = if (isEnable) 1.0f else 0.5f
        binding.outBreakParent.isUserInteractionEnabled(isEnable)

        //Reset Signed in text time and total break duration
        if (!isEnable) {
            viewModel.inTime.set("09:30 AM")
            viewModel.breakTime.set("00:00:00")
            breakTimeOffset = 0
            elapsedTotaltime = 0
        }
    }

    //disable/enable a view and all of it's children
    private fun View.isUserInteractionEnabled(enabled: Boolean) {
        isEnabled = enabled
        if (this is ViewGroup && this.childCount > 0) {
            this.children.forEach {
                it.isUserInteractionEnabled(enabled)
            }
        }
    }

    /**With this No observer needed in activity*/
    private fun registerObservables() {
        viewModel.getNavigationEvent().setEventReceiver(this, this)
    }

    override fun startInTimer(isInTimerWorking: Boolean) {
        if (isInTimerWorking) {
            binding.inTimer.base = SystemClock.elapsedRealtime() - 0
            binding.inTimer.start()
            viewModel.inClockVisibility.set(View.GONE)
            viewModel.inTimerVisibility.set(View.VISIBLE)
        } else {
            binding.inTimer.stop()
            viewModel.inClockVisibility.set(View.VISIBLE)
            viewModel.inTimerVisibility.set(View.GONE)
        }
    }

    override fun startBreakTimer(isBreakTimerWorking: Boolean) {
        if (isBreakTimerWorking) {
            binding.breakTimer.base = SystemClock.elapsedRealtime() - 0
            binding.breakTimer.start()
            viewModel.breakClockVisibility.set(View.GONE)
            viewModel.breakTimerVisibility.set(View.VISIBLE)
            breakTimeOffset = binding.breakTimer.getBase()
        } else {
            binding.breakTimer.stop()
            viewModel.breakClockVisibility.set(View.VISIBLE)
            viewModel.breakTimerVisibility.set(View.GONE)
            breakTimeOffset = SystemClock.elapsedRealtime() - binding.breakTimer.base
            elapsedTotaltime = elapsedTotaltime + breakTimeOffset
            viewModel.breakTime.set(formatInterval(elapsedTotaltime))
        }
    }

    private fun formatInterval(seconds: Long): String? {
        val hr: Long = TimeUnit.MILLISECONDS.toHours(seconds)
        val min: Long = TimeUnit.MILLISECONDS.toMinutes(seconds - TimeUnit.HOURS.toMillis(hr))
        val sec: Long = TimeUnit.MILLISECONDS.toSeconds(
            seconds - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min)
        )
        return String.format("%02d:%02d:%02d", hr, min, sec)
    }

    override fun showToast(message: String) {

    }

    override fun onClick(view: View) {

    }

    override fun showDialogForClockIn(value: Boolean) {
       val discardTimerFragment = DialogFragment.newInstance(
           "Today's Task List",
           "Your task for today"
       );
        discardTimerFragment.setDialogListener(object : DialogFragment.DialogListener {
            override fun onCancelClick() {
                //TODO
            }

            override fun onOkClick(message: String) {
                //TODO message post on server
                startInTimer(true)
                enableDisableClock(true)
                //Update In time
                val time = SimpleDateFormat(
                    "hh:mm a",
                    Locale.getDefault()
                ).format(Calendar.getInstance().time)
                viewModel.inTime.set(time)
            }
        })
        discardTimerFragment.show(requireActivity().supportFragmentManager, "Dialog")
    }

    override fun showDialogForClockOut(value: Boolean) {
       val discardTimerFragment = DialogFragment.newInstance(
           "Reason",
           "You are leaving(Clock OUT) before working hours(9 hrs).\n Please tell us a reason and Clock OUT."
       );
        discardTimerFragment.setDialogListener(object : DialogFragment.DialogListener {
            override fun onCancelClick() {
                //TODO
            }

            override fun onOkClick(message: String) {
                //TODO message post on server
                startInTimer(false)
                startBreakTimer(false)
                enableDisableClock(false)
            }
        })
        discardTimerFragment.show(requireActivity().supportFragmentManager, "Dialog")
    }
}