package com.android.vsshr.home

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.android.vsshr.R
import com.android.vsshr.databinding.FragmentOfficialLeavesBinding
import com.android.vsshr.databinding.FragmentUnPlannedLeavesBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [OfficialLeavesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class OfficialLeavesFragment : Fragment() {

    private lateinit var binding: FragmentOfficialLeavesBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_official_leaves, container, false)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        for(i in 0..10){
            val item = View.inflate(context, R.layout.offical_leaves_item, null)
            binding.container.addView(item,i)
        }
    }
}