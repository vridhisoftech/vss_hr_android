package com.android.vsshr.home.notifications

import android.view.View
import androidx.databinding.ObservableInt
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.Disposable

class NotificationsViewModel(private val repository: NotificationsRepository) : ViewModel() {

    private var disposable: Disposable? = null
    var progress: ObservableInt = ObservableInt(View.GONE)
    var noData: ObservableInt = ObservableInt(View.GONE)

    override fun onCleared() {
        disposable?.dispose()
        super.onCleared()
    }

}