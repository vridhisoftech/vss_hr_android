package com.android.vsshr.home.notifications

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.vsshr.R
import com.android.vsshr.databinding.FragmentNotificationsBinding
import com.android.vsshr.factory.ViewModelFactory
import com.android.vsshr.home.model.Data
import com.android.vsshr.home.model.ViewList
import com.android.vsshr.utils.Utils
import com.android.vsshr.utils.snackBar
import com.android.vsshr.utils.toast
import kotlinx.android.synthetic.main.fragment_notifications.*

class NotificationsFragment : Fragment() {

    private lateinit var viewModel: NotificationsViewModel
    private lateinit var binding: FragmentNotificationsBinding

    private lateinit var adapter: NotificationListAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_notifications, container, false)
        // With ViewModelFactory
        viewModel = ViewModelProvider(this@NotificationsFragment, ViewModelFactory()).get(
            NotificationsViewModel::class.java
        )

        binding.viewModel = viewModel

        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        swipeRefresh.setOnRefreshListener {
            refresh()
        }

        setRecyclerViewHome(getHomeListData()[0].viewList)
    }



    private fun refresh() {
        swipeRefresh.isRefreshing = true
        setRecyclerViewHome(getHomeListData()[0].viewList)
    }

    private fun getHomeListData(): List<Data> {
        return Utils.getCollectionsList(requireContext())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 2) {
            refresh()
        }
    }

    private fun setRecyclerViewHome(dataList: List<ViewList>) {
        recycler_view_home.setHasFixedSize(true)
        adapter = NotificationListAdapter(requireActivity(), dataList)
        val categoryLinearLayoutManager = LinearLayoutManager(context)
        categoryLinearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recycler_view_home.layoutManager = categoryLinearLayoutManager
        recycler_view_home.adapter = adapter
        swipeRefresh.isRefreshing = false
        // recycler_view_home.setHasFixedSize(true)
    }

    companion object {
        private const val categoryName = "param1"
        fun newInstance(param1: String): NotificationsFragment {
            val fragment = NotificationsFragment()
            val args = Bundle()
            args.putString(categoryName, param1)
            fragment.arguments = args
            return fragment
        }
    }

}

