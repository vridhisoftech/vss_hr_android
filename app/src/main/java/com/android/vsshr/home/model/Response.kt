package com.android.vsshr.home.model

data class Response(
    val `data`: List<Data>,
    val message: String,
    val status: Int
)