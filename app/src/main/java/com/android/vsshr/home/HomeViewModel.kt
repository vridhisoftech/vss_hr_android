package com.android.vsshr.home

import android.view.View
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.ViewModel
import com.android.vsshr.App
import com.android.vsshr.liveobserver.LiveMessageEvent
import io.reactivex.disposables.Disposable
import java.text.SimpleDateFormat
import java.util.*

class HomeViewModel(private val repository: HomeRepository) : ViewModel() {

    private var disposable: Disposable? = null
    private var navigationEvent: LiveMessageEvent<HomeCommand> = LiveMessageEvent()
    var inClockVisibility: ObservableInt = ObservableInt(View.VISIBLE)
    var inTimerVisibility: ObservableInt = ObservableInt(View.GONE)
    var inTime: ObservableField<String> = ObservableField("09:30 AM")
    var breakTime: ObservableField<String> = ObservableField("00:00:00")
    var breakClockVisibility: ObservableInt = ObservableInt(View.VISIBLE)
    var breakTimerVisibility: ObservableInt = ObservableInt(View.GONE)

    var userName: ObservableField<String> = ObservableField("Nasir Ali")
    var empId: ObservableField<String> = ObservableField("VSS-007")
    var designation: ObservableField<String> = ObservableField("Software Engineer")
    var location: ObservableField<String> = ObservableField("Location: Faridabad")

    override fun onCleared() {
        disposable?.dispose()
        super.onCleared()
    }

    fun getNavigationEvent(): LiveMessageEvent<HomeCommand> {
        return navigationEvent
    }

    fun getHomeListItems() {
        if (App.getInstance().isConnected()) {
            //disposable?.dispose()//dispose pre call
            /*disposable = repository.fetchHomeData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    navigationEvent.sendEvent { homeFeedList(result) }
                    progress.set(View.GONE)
                    if (result?.data!!.isEmpty()) {
                        noData.set(View.VISIBLE)
                    }
                }, { error ->
                    navigationEvent.sendEvent { showToast(error.message!!) }
                })*/
        } else {
            navigationEvent.sendEvent { showToast("Please connect to Internet!") }
        }
    }

    fun onLongClickOnClockIn(): Boolean {
        if(inClockVisibility.get().equals(View.VISIBLE)){
            navigationEvent.sendEvent { showDialogForClockIn(true) }
        }
        return true
    }

    fun onLongClickOnClockOut(): Boolean {
        navigationEvent.sendEvent { showDialogForClockOut(true) }
        return true
    }

    fun onLongClickOnBreakStart(): Boolean {
        if(breakClockVisibility.get().equals(View.VISIBLE)){
            navigationEvent.sendEvent { startBreakTimer(true) }
        }else{
            navigationEvent.sendEvent { startBreakTimer(false) }
        }
        return true
    }

}