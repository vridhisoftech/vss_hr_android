package com.android.vsshr.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.android.vsshr.R
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MyLeavesFragment : Fragment() {

    private lateinit var viewPager: ViewPager2
    private lateinit var tabLayout: TabLayout

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_my_leaves, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        tabLayout = view.findViewById(R.id.tab_layout)
        viewPager = view.findViewById(R.id.pager)

        viewPager.adapter = TabPagerAdapter(requireActivity().supportFragmentManager, lifecycle)

        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when (position) {
                0 -> {
                    tab.text = "Planned"
                    tab.icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_planned)
                    tab.icon?.colorFilter =
                        BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
                            android.R.color.white,
                            BlendModeCompat.SRC_ATOP
                        )
                }
                1 -> {
                    tab.text = "Unplanned"
                    tab.icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_unplaned)
                    tab.icon?.colorFilter =
                        BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
                            android.R.color.white,
                            BlendModeCompat.SRC_ATOP
                        )
                }
            }
        }.attach()

    }

    class TabPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) : FragmentStateAdapter(fragmentManager, lifecycle)  {

        override fun createFragment(position: Int): Fragment {
            return when (position) {
                0 -> PlannedFragment.getInstance()
                1 -> UnPlannedFragment.getInstance()
                else -> PlannedFragment.getInstance()
            }
        }

        override fun getItemCount(): Int {
            return 2
        }
    }
}