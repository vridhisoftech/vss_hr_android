package com.android.vsshr.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.android.vsshr.R
import com.android.vsshr.databinding.FragmentGatePassBinding
import com.android.vsshr.databinding.FragmentOutOfOfficeBinding
import com.android.vsshr.utils.toast

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class OutOfOfficeFragment : Fragment() {
    private lateinit var binding: FragmentOutOfOfficeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_out_of_office, container, false)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        for(i in 0..4){
            val item = View.inflate(context, R.layout.out_of_office_item, null)
            binding.container.addView(item,i)
        }
    }

    companion object {
        fun getInstance(): GatePassFragment {
            val mInstance: GatePassFragment by lazy { GatePassFragment() }//by lazy single object created for this class
            return mInstance
        }
    }
}