package com.android.vsshr.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.android.vsshr.R
import com.android.vsshr.databinding.FragmentGatePassBinding
import com.android.vsshr.databinding.FragmentPlannedLeavesBinding
import com.android.vsshr.utils.goActivity
import com.android.vsshr.utils.toast

class GatePassFragment : Fragment() {

    private lateinit var binding: FragmentGatePassBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_gate_pass, container, false)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.newRequest.setOnClickListener {
            requireActivity().goActivity(GatePassRequestActivity::class.java)
        }
        for(i in 0..4){
            val item = View.inflate(context, R.layout.gate_pass_item, null)
            binding.container.addView(item,i)
        }
    }

    companion object {
        fun getInstance(): GatePassFragment {
            val mInstance: GatePassFragment by lazy { GatePassFragment() }//by lazy single object created for this class
            return mInstance
        }
    }

}