package com.android.vsshr.home

import com.android.vsshr.network.WebCall
import io.reactivex.Observable


class HomeRepository {

    /**The singleton BackEndApi object that is created lazily when the first time it is used
     * After that it will be reused without creation
     */
    private val apiServices by lazy { WebCall.create() }


    companion object {
        fun getInstance(): HomeRepository {
            val mInstance: HomeRepository by lazy { HomeRepository() }
            return mInstance
        }
    }
}