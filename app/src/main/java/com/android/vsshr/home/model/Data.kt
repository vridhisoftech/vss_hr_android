package com.android.vsshr.home.model

data class Data(
    val viewList: List<ViewList>,
    val viewType: String,
    val label: String
)