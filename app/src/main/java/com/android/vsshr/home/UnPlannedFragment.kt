package com.android.vsshr.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.android.vsshr.R
import com.android.vsshr.databinding.FragmentOfficialLeavesBinding
import com.android.vsshr.databinding.FragmentUnPlannedLeavesBinding

class UnPlannedFragment : Fragment() {

    private lateinit var binding: FragmentUnPlannedLeavesBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_un_planned_leaves, container, false)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        for(i in 0..10){
            val item = View.inflate(context, R.layout.leaves_item, null)
            binding.container.addView(item,i)
        }
    }

    companion object {
        fun getInstance(): UnPlannedFragment {
            val mInstance: UnPlannedFragment by lazy { UnPlannedFragment() }//by lazy single object created for this class
            return mInstance
        }
    }
}