package com.android.vsshr.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.android.vsshr.R
import com.android.vsshr.auth.AuthRepository
import com.android.vsshr.databinding.FragmentPlannedLeavesBinding
import com.android.vsshr.planleaves.PlanLeavesActivity
import com.android.vsshr.utils.goActivity
import com.android.vsshr.utils.toast

//Planned Leaves
class PlannedFragment : Fragment() {


    private lateinit var binding: FragmentPlannedLeavesBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_planned_leaves, container, false)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.addPlannedLeave.setOnClickListener {
            requireActivity().goActivity(PlanLeavesActivity::class.java)
        }

        for(i in 0..10){
            val item = View.inflate(context, R.layout.leaves_item, null)
            binding.container.addView(item,i)
        }
    }

    companion object {
        fun getInstance(): PlannedFragment {
            val mInstance: PlannedFragment by lazy { PlannedFragment() }//by lazy single object created for this class
            return mInstance
        }
    }
}