package com.android.vsshr.home.model

data class ViewList(
    val catName: String = "",
    val imgUrl: String
)