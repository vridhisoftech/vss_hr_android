package com.android.vsshr.home

import android.view.View

interface HomeCommand {
    fun startInTimer(value: Boolean)
    fun startBreakTimer(value: Boolean)
    fun showToast(message: String)
    fun onClick(view: View)
    fun showDialogForClockOut(value: Boolean)
    fun showDialogForClockIn(value: Boolean)
}