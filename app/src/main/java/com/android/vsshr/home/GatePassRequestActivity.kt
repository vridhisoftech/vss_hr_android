package com.android.vsshr.home

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.android.vsshr.R
import com.android.vsshr.databinding.ActivityGatePassRequestBinding
import com.android.vsshr.planleaves.PlanLeavesActivity
import com.android.vsshr.utils.goActivity
import kotlinx.android.synthetic.main.activity_gate_pass_request.*
import kotlinx.android.synthetic.main.activity_plan_leaves.*
import kotlinx.android.synthetic.main.activity_plan_leaves.txt_selectedEndDate
import kotlinx.android.synthetic.main.activity_plan_leaves.txt_selectedStartDate
import java.util.*

class GatePassRequestActivity : AppCompatActivity() {

    private lateinit var binding: ActivityGatePassRequestBinding
    var selectedDate:String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_gate_pass_request)
        supportActionBar!!.title = "New Gate Pass"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        binding.btnSave.setOnClickListener {
            finish()
        }
    }
    fun gateStartedDate(v: View) {
        datePickerStartDate()
    }

    fun gateEndDate(v: View) {
        datePickerEndDate()
    }
    fun gateStartTime(v:View)
    {
        timePicker()
    }
    fun gateEndTime(v:View)
    {
        endTimePicker()
    }


    @SuppressLint("SimpleDateFormat")
    private fun timePicker() {
        val mCurrentTime = Calendar.getInstance()
        val hour = mCurrentTime.get(Calendar.HOUR_OF_DAY)
        val minute = mCurrentTime.get(Calendar.MINUTE)

        val timePickerDialog = TimePickerDialog(this,R.style.DialogTheme,
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                onTimeSet(hourOfDay,minute)

            }, hour, minute, false)
        timePickerDialog.show()

    }

    fun onTimeSet( hourOfDay: Int, minute: Int) {
        var am_pm = ""
        val datetime = Calendar.getInstance()
        datetime[Calendar.HOUR_OF_DAY] = hourOfDay
        datetime[Calendar.MINUTE] = minute
        if (datetime[Calendar.AM_PM] == Calendar.AM) am_pm = "AM" else if (datetime[Calendar.AM_PM] == Calendar.PM) am_pm = "PM"
        val strHrsToShow =
            if (datetime[Calendar.HOUR] == 0) "12" else datetime[Calendar.HOUR].toString() + ""
        txt_selectedStartTime.text = "$strHrsToShow:" + datetime[Calendar.MINUTE] + " " + am_pm

    }
    @SuppressLint("SimpleDateFormat")
    private fun endTimePicker() {
        val mCurrentTime = Calendar.getInstance()
        val hour = mCurrentTime.get(Calendar.HOUR_OF_DAY)
        val minute = mCurrentTime.get(Calendar.MINUTE)

        val timePickerDialog = TimePickerDialog(this,R.style.DialogTheme,
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                onEndTimeSet(hourOfDay,minute)

            }, hour, minute, false)
        timePickerDialog.show()

    }

    fun onEndTimeSet( hourOfDay: Int, minute: Int) {
        var am_pm = ""
        val datetime = Calendar.getInstance()
        datetime[Calendar.HOUR_OF_DAY] = hourOfDay
        datetime[Calendar.MINUTE] = minute
        if (datetime[Calendar.AM_PM] == Calendar.AM) am_pm = "AM" else if (datetime[Calendar.AM_PM] == Calendar.PM) am_pm = "PM"
        val strHrsToShow =
            if (datetime[Calendar.HOUR] == 0) "12" else datetime[Calendar.HOUR].toString() + ""
        txt_selectedEndTime.text = "$strHrsToShow:" + datetime[Calendar.MINUTE] + " " + am_pm

    }


    private fun datePickerStartDate(){
        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(
            this, R.style.DialogTheme,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                selectedDate = (dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
                txt_selectedStartDate.text = selectedDate
            }, mYear, mMonth, mDay
        )
        datePickerDialog.show()
    }

    private fun datePickerEndDate() {
        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(
            this, R.style.DialogTheme,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                selectedDate = (dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
                txt_selectedEndDate.text = selectedDate

            }, mYear, mMonth, mDay
        )
        datePickerDialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when {
            item.itemId == android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}