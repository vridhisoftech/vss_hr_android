package com.android.vsshr.dialog.leavetypedialog

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.vsshr.databinding.LeaveTypeDialogAdapterBinding
import kotlinx.android.synthetic.main.leave_type_dialog_adapter.view.*

class LeaveTypeListAdapter(
    private val itemList: List<LeaveTypeModel>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
var lastSelectedPosition=0
    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val feeder = itemList[position]
        (holder as ListViewHolder).bind(feeder)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ListViewHolder(
            LeaveTypeDialogAdapterBinding.inflate(layoutInflater, parent, false)
        )
    }

    private inner class ListViewHolder(private var applicationBinding: LeaveTypeDialogAdapterBinding) :
        RecyclerView.ViewHolder(applicationBinding.root) {

        fun bind(feed: LeaveTypeModel) {
            applicationBinding.viewModel = feed
            itemView.male.isChecked = lastSelectedPosition == adapterPosition;
            itemView.male.setOnClickListener {
                lastSelectedPosition = adapterPosition;
                notifyDataSetChanged()
            }
        }
    }

}


