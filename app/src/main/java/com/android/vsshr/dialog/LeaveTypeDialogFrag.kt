package com.android.vsshr.home

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.vsshr.R
import com.android.vsshr.dialog.leavetypedialog.LeaveTypeListAdapter
import com.android.vsshr.dialog.leavetypedialog.LeaveTypeModel
import kotlinx.android.synthetic.main.fragment_leave_type_dialog.*
import kotlinx.android.synthetic.main.fragment_leave_type_dialog.view.*

class LeaveTypeDialogFrag : DialogFragment() {
    var dataList = ArrayList<LeaveTypeModel>()
    private lateinit var adapter: LeaveTypeListAdapter
    private lateinit var dialogListener: DialogListener

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_leave_type_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupClickListeners(view)
        prepareData()
        setRecyclerView(dataList)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog?.window?.decorView?.setBackgroundColor(Color.TRANSPARENT)
    }
    private fun prepareData() {
        var dataPlanning = LeaveTypeModel("Casual Leave")
        dataList.add(dataPlanning)

        dataPlanning = LeaveTypeModel("Public Holiday")
        dataList.add(dataPlanning)

        dataPlanning = LeaveTypeModel("Sick Leave")
        dataList.add(dataPlanning)
    }

    private fun setRecyclerView(dataList: ArrayList<LeaveTypeModel>) {
        adapter = LeaveTypeListAdapter( dataList)
        val categoryLinearLayoutManager = LinearLayoutManager(requireContext())
        categoryLinearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        chooseLeaveTypeRecyclerView.layoutManager = categoryLinearLayoutManager
        chooseLeaveTypeRecyclerView.adapter = adapter

    }
    private fun setupClickListeners(view: View) {
        view.btnOk.setOnClickListener {
            dismiss()
        }
        view.btnCancel.setOnClickListener {
            dismiss()
        }
    }

    interface DialogListener{
        fun onCancelClick()
        fun onOkClick(str : String)
    }

    fun setDialogListener(listner: DialogListener){
        dialogListener = listner
    }

}