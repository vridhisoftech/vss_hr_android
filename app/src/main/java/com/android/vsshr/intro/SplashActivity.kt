package com.android.vsshr.intro

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.android.vsshr.R
import com.android.vsshr.auth.LoginActivity
import com.android.vsshr.utils.goActivity


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed(Runnable {
            this.goActivity(LoginActivity::class.java)
            finish()
        }, 3000)

    }
}
