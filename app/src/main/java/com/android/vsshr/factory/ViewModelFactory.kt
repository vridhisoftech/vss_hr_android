package com.android.vsshr.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.vsshr.auth.LoginViewModel
import com.android.vsshr.auth.AuthRepository
import com.android.vsshr.auth.SignUpViewModel
import com.android.vsshr.home.HomeRepository
import com.android.vsshr.home.HomeViewModel
import com.android.vsshr.home.notifications.NotificationsRepository
import com.android.vsshr.home.notifications.NotificationsViewModel
import com.android.vsshr.planleaves.PlanningLeaveRepository
import com.android.vsshr.planleaves.PlanningLeaveViewModel

class ViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
            HomeViewModel(HomeRepository.getInstance()) as T
        }else if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            LoginViewModel(AuthRepository.getInstance()) as T
        }else if (modelClass.isAssignableFrom(SignUpViewModel::class.java)) {
            SignUpViewModel(AuthRepository.getInstance()) as T
        } else if (modelClass.isAssignableFrom(NotificationsViewModel::class.java)) {
            NotificationsViewModel(NotificationsRepository.getInstance()) as T
        } else if (modelClass.isAssignableFrom(PlanningLeaveViewModel::class.java)) {
            PlanningLeaveViewModel(PlanningLeaveRepository.getInstance()) as T
        }  else {
            //TODO for others view model repository
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}
