package com.android.vsshr.auth

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.android.vsshr.R
import com.android.vsshr.databinding.ActivitySignupBinding
import com.android.vsshr.factory.ViewModelFactory
import com.android.vsshr.utils.goActivity
import com.android.vsshr.utils.toast

class SignUpActivity : AppCompatActivity(), AuthListener {

    private val TAG = "PublicLogin"
    private lateinit var viewModel: SignUpViewModel
    private lateinit var binding: ActivitySignupBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup)
        viewModel = ViewModelProvider(
            this@SignUpActivity,
            ViewModelFactory()
        ).get(SignUpViewModel::class.java)
        binding.model = viewModel
        registerObservables()

        //fname.leftDrawable(R.drawable.ic_username, R.dimen.drawable_icon_size)
        //mobile.leftDrawable(R.drawable.ic_call, R.dimen.drawable_icon_size)
        //email.leftDrawable(R.drawable.ic_mail, R.dimen.drawable_icon_size)
        //password.leftDrawable(R.drawable.ic_pass, R.dimen.drawable_icon_size)
        //zip_code.leftDrawable(R.drawable.ic_pass, R.dimen.drawable_icon_size)
        //age.leftDrawable(R.drawable.ic_mail, R.dimen.drawable_icon_size)

    }

    /**With this No observer needed in activity*/
    private fun registerObservables() {
        viewModel.getNavigationEvent().setEventReceiver(this, this)
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onSuccess(message: String) {
        this.toast(message)
        this.goActivity(SignUpActivity::class.java)
        finish()
    }

    override fun onFailure(message: String) {
        this.toast(message)
    }

    override fun <T> openActivity(activity: Class<T>) {
        this.goActivity(activity)
        finish()
    }


    override fun onBackPressed() {
        this.goActivity(LoginActivity::class.java)
        super.onBackPressed()
    }

}
