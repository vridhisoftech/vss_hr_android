package com.android.vsshr.auth

interface AuthListener {
    fun showToast(message: String)
    fun onSuccess(message: String)
    fun onFailure(message: String)
    fun <T> openActivity(activity: Class<T>)
}