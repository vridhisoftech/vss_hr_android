package com.android.vsshr.auth

import com.android.vsshr.network.WebCall
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */
class AuthRepository {

    /**The singleton BackEndApi object that is created lazily when the first time it is used
     * After that it will be reused without creation
     */
    private val apiService by lazy { WebCall.create() }

    companion object {
        fun getInstance(): AuthRepository {
            val mInstance: AuthRepository by lazy { AuthRepository() }//by lazy single object created for this class
            return mInstance
        }
    }

}
