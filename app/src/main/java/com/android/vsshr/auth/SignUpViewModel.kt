package com.android.vsshr.auth

import android.text.TextUtils
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.android.vsshr.App
import com.android.vsshr.home.HomeActivity
import com.android.vsshr.liveobserver.LiveMessageEvent
import com.android.vsshr.utils.Utils
import io.reactivex.disposables.Disposable
import okhttp3.MediaType
import okhttp3.RequestBody

class SignUpViewModel(private val repository: AuthRepository) : ViewModel() {

    private var disposable: Disposable? = null
    private var navigationEvent: LiveMessageEvent<AuthListener> = LiveMessageEvent()
    var name: ObservableField<String> = ObservableField("")
    var employeeId: ObservableField<String> = ObservableField("")
    var password: ObservableField<String> = ObservableField("")

    override fun onCleared() {
        disposable?.dispose()
        super.onCleared()
    }


    fun getNavigationEvent(): LiveMessageEvent<AuthListener> {
        return navigationEvent
    }

    /* fun onUserNameChanged(text: CharSequence, start: Int, before: Int, count: Int) {

     }*/

    fun onEmailTextChanged(text: CharSequence, start: Int, before: Int, count: Int) {
        if (text.isNotEmpty()) {
            if (isValidEmail(text) /*|| isNotValidMobile(text)*/) {
                setError(false, INPUT_EMAIL)
            } else {
                setError(true, INPUT_EMAIL)
            }
        }
    }

    /* fun onMobileNoChanged(text: CharSequence, start: Int, before: Int, count: Int) {

     }*/

    fun onPasswordTextChanged(text: CharSequence, start: Int, before: Int, count: Int) {
        if (text.isNotEmpty()) {
            if (isPassValid(text)) {
                setError(false, INPUT_PASS)
            } else {
                setError(true, INPUT_PASS)
            }
        }
    }

    private fun isValidEmail(email: CharSequence): Boolean {
        /*Min a@b.cc*/
        var isValid = false
        if (email.isEmpty()) {
            navigationEvent.sendEvent { showToast("Enter an email!") }
        } else if (Utils.isEmailValid(email)) {
            isValid = true
        }
        return isValid
    }

    private fun isPassValid(password: CharSequence): Boolean {
        var isValid = false
        if (password.isEmpty()) {
            navigationEvent.sendEvent { showToast("Enter a password!") }
        } else if (password.length > 7) {
            isValid = true
        }
        return isValid
    }

    private fun setError(isError: Boolean, inputType: String) {
        navigationEvent.sendEvent { setError(isError, inputType) }
    }

    fun openDatePicker() {
        navigationEvent.sendEvent { openDatePicker() }
    }


    fun enterSignUp() {
        navigationEvent.sendEvent { openActivity(HomeActivity::class.java) }
         return
        if (App.getInstance().isConnected()) {
            val name = name.get()!!
            val employeeId = employeeId.get()!!
            val password = password.get()!!

            if (TextUtils.isEmpty(name)) {
                navigationEvent.sendEvent { showToast("Name filed is invalid!") }
                return
            } else if (TextUtils.isEmpty(employeeId)) {
                navigationEvent.sendEvent { showToast("Employee-Id filed is invalid!") }
                return
            } else if (TextUtils.isEmpty(password) || !isPassValid(password)) {
                navigationEvent.sendEvent { showToast("Password filed is invalid, min length 8!") }
                return
            }

            disposable?.dispose()//dispose  pre call

            try {

                val name: RequestBody = createRequestBody(name)
                val employeeId: RequestBody = createRequestBody(employeeId)
                val password: RequestBody = createRequestBody(password)

                val params: HashMap<String, RequestBody> = HashMap()
                params["name"] = name
                params["employeeId"] = employeeId
                params["password"] = password

                /*disposable = repository.signUp(params, fileBody)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ result ->
                        sendSignUpData(result)
                        repository.setLoggedInUser(result)
                        notifyProgressVisibility(View.GONE)
                    }, { error ->
                        notifyProgressVisibility(View.GONE)
                        navigationEvent.sendEvent { showToast(error.message!!) }
                    })*/

            } catch (e: Exception) {
                navigationEvent.sendEvent { showToast(e.message!!) }
            }
        } else {
            navigationEvent.sendEvent { showToast("Something went wrong internet or data!") }
        }
    }

    fun createRequestBody(s: String): RequestBody {
        return RequestBody.create(MediaType.parse("multipart/form-data"), s)
    }

}