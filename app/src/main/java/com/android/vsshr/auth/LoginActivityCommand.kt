package com.android.vsshr.auth

import android.app.Activity

interface LoginActivityCommand {
    fun showToast(message: String)
    fun setError(isError: Boolean,inputType: String)
    fun <T> openActivity(activity: Class<T>)
}