package com.android.vsshr.auth

import android.graphics.Color
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.android.vsshr.R
import com.android.vsshr.auth.INPUT_EMAIL
import com.android.vsshr.auth.INPUT_PASS
import com.android.vsshr.auth.LoginActivityCommand
import com.android.vsshr.auth.LoginViewModel
import com.android.vsshr.databinding.ActivityLoginBinding
import com.android.vsshr.factory.ViewModelFactory
import com.android.vsshr.utils.goActivity

class LoginActivity : AppCompatActivity(), LoginActivityCommand {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewmodel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupBindings(savedInstanceState)
    }

    private fun setupBindings(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
            viewmodel = ViewModelProvider(this, ViewModelFactory()).get(LoginViewModel::class.java)
            binding.model = viewmodel
            registerObservables()
        }

    }

    /**With this No observer needed in activity*/
    private fun registerObservables() {
        viewmodel.getNavigationEvent().setEventReceiver(this, this)
    }


    override fun setError(isError: Boolean, inputType: String) {
        if (isError) {
            if (inputType == INPUT_EMAIL) setError(binding.etEmail)
            else if (inputType == INPUT_PASS) setError(binding.etPass)
        } else {
            if (inputType == INPUT_EMAIL) removeError(binding.etEmail)
            else if (inputType == INPUT_PASS) removeError(binding.etPass)
        }
    }

    private fun removeError(text: TextView) {
        text.setTextColor(Color.BLACK)
        text.setHintTextColor(Color.BLACK)
    }

    private fun setError(text: TextView) {
        text.setTextColor(Color.RED)
        text.setHintTextColor(Color.RED)
    }


    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }


    override fun <T> openActivity(activity: Class<T>) {
        this.goActivity(activity)
        finish()
    }

}