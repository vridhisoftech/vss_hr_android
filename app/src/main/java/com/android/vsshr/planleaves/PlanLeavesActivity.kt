package com.android.vsshr.planleaves

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NavUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.vsshr.R
import com.android.vsshr.databinding.ActivityPlanLeavesBinding
import com.android.vsshr.factory.ViewModelFactory
import com.android.vsshr.home.LeaveTypeDialogFrag
import com.android.vsshr.planleaves.model.PlanningLeaveModel
import kotlinx.android.synthetic.main.activity_plan_leaves.*
import kotlinx.android.synthetic.main.activity_plan_leaves.view.*
import java.util.*
import kotlin.collections.ArrayList

class PlanLeavesActivity : AppCompatActivity() {
    var selectedDate: String? = null
    private lateinit var viewModel: PlanningLeaveViewModel
    private lateinit var binding: ActivityPlanLeavesBinding
    var dataList = ArrayList<PlanningLeaveModel>()

    private lateinit var adapter: PlanningLeaveListAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_plan_leaves)
        viewModel = ViewModelProvider(this, ViewModelFactory()).get(PlanningLeaveViewModel::class.java)
        binding.viewModel = viewModel
        supportActionBar!!.title="Add Planned Leaves"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        prepareData()
        setRecyclerView(dataList)
        binding.root.edtComment.setText("Lorem Ipsum is simply dummy text of the printing and typesetting industry.")

        binding.root.cardView_leave_type.setOnClickListener {
            val leaveTypeDialogFragment= LeaveTypeDialogFrag()
            leaveTypeDialogFragment.show(supportFragmentManager, "")
        }

        binding.root.btnSave.setOnClickListener { onBackPressed() }
    }

    private fun prepareData() {
        var dataPlanning = PlanningLeaveModel("Starting Balance", 26)
        dataList.add(dataPlanning)

        dataPlanning = PlanningLeaveModel("Earned Leaves(Incl. Comp Off)", 4)
        dataList.add(dataPlanning)
        dataPlanning = PlanningLeaveModel("Casual Leaves", 5)
        dataList.add(dataPlanning)
        dataPlanning = PlanningLeaveModel("Sick Leaves", 0)
        dataList.add(dataPlanning)
        dataPlanning = PlanningLeaveModel("Public Holidays", 3)
        dataList.add(dataPlanning)
        dataPlanning = PlanningLeaveModel("Penalty Leaves", 5)
        dataList.add(dataPlanning)
        dataPlanning = PlanningLeaveModel("Total Leaves Taken", 6)
        dataList.add(dataPlanning)
        dataPlanning = PlanningLeaveModel("Annual Leave Taken", 9)
        dataList.add(dataPlanning)
        dataPlanning = PlanningLeaveModel("Earned Leave Balance", 5)
        dataList.add(dataPlanning)
    }

    private fun setRecyclerView(dataList: ArrayList<PlanningLeaveModel>) {
        adapter = PlanningLeaveListAdapter(this, dataList)
        val categoryLinearLayoutManager = LinearLayoutManager(this)
        categoryLinearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        binding.root.leaveTypesRecyclerView.layoutManager = categoryLinearLayoutManager
        binding.root.leaveTypesRecyclerView.adapter = adapter

    }

    fun setStartedDate(v: View) {
        datePickerStartDate()
    }

    fun setEndDate(v: View) {
datePickerEndDate()
    }

    private fun datePickerStartDate(){
        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(
            this, R.style.DialogTheme,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                selectedDate = (dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
                txt_selectedStartDate.text = selectedDate
            }, mYear, mMonth, mDay
        )
        datePickerDialog.show()
    }

    private fun datePickerEndDate() {
        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(
            this, R.style.DialogTheme,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                selectedDate = (dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
                txt_selectedEndDate.text = selectedDate

            }, mYear, mMonth, mDay
        )
        datePickerDialog.show()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when {
            item.itemId == android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}