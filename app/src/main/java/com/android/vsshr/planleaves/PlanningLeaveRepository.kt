package com.android.vsshr.planleaves

import com.android.vsshr.network.WebCall
import io.reactivex.Observable

class PlanningLeaveRepository {

    /**The singleton BackEndApi object that is created lazily when the first time it is used
     * After that it will be reused without creation
     */
    private val apiServices by lazy { WebCall.create() }


    companion object {
        fun getInstance(): PlanningLeaveRepository {
            val mInstance: PlanningLeaveRepository by lazy { PlanningLeaveRepository() }
            return mInstance
        }
    }
}